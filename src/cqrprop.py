import gi
gi.require_version('GdkPixbuf', '2.0')
from gi.repository import GdkPixbuf, GLib
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk, Gdk
from PIL import Image
import requests

imggif = Image.open(requests.get("https://www.hamqsl.com/solar101pic.php", stream=True).raw)
imggif.load()
img = Image.new("RGB", imggif.size, (255, 255, 255))
img.paste(imggif)

xpos = 1500
ypos = 50

class ShowPortrait(Gtk.Window):

    def __init__(self):
        Gtk.Window.__init__(self, title="CQRPROP")
        self.set_type_hint(Gdk.WindowTypeHint.DESKTOP)
        self.connect("destroy", Gtk.main_quit)
        self.set_skip_taskbar_hint(True)
        data = img.tobytes()
        w, h = img.size
        data = GLib.Bytes.new(data)
        pixbuf = GdkPixbuf.Pixbuf.new_from_bytes(data, GdkPixbuf.Colorspace.RGB,
                                                 False, 8, w, h, w * 3)
        image = Gtk.Image.new_from_pixbuf(pixbuf.copy())
        self.add(image)
        self.move(xpos, ypos)
        self.show_all()


ShowPortrait()
Gtk.main()
