# yahpt

Yet Another HF Propagation Tool.

A small application that shows propagation data from Paul, N0NBH website http://www.hamqsl.com/solar.html on a Linux desktop, written in Python.

[Original maintainer: F4IQN](https://gist.github.com/Maxime-Favier/1489669a1ec4dedf0289113d6ffdc768)


Screenshots:
------------

![Alt text](https://media.discordapp.net/attachments/719267891694010389/893922865764765807/unknown.png?width=1000&height=532 "Main window")
